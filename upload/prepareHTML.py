def prepareInstructions():
    #TODO Automate these parts
    htmlInstructions =  "<title>Automatic Text Segmentation</title>\n"
    htmlInstructions +=  "<h4>\n"
    htmlInstructions +=  "Steps for Automatic Text Segmentation <br>\n"
    htmlInstructions += "1. Upload File*<br>\n"
    htmlInstructions += "<font color=\"blue\">2. Manually Segment and Train System </font><br>\n"
    htmlInstructions += "3. Run the system to Segment the remaining Examples <br>\n"
    htmlInstructions +=  "</h4>\n"

def includeJSFile(jsFile):
    jsFileInclude = "<script src=\"/JS/"+ jsFile+"\"></script>\n"
    return jsFileInclude

def includeJSLibraries():
    jsLib = "<script src=\"/JS/angular/angular.min.js\"></script>\n"
    jsLib += "<script src=\"/JS/angular/angular-sanitize.js\"></script>\n"
    return jsLib

def prepareDiv(textContent, divName, mouseClick):
    if not mouseClick:
      return "<div id=\"" + divName + "\">" + textContent + "</div>"
    else:
      return "<div id=\"" + divName + "\" ng-mouseup=\"selectionMade($event)\">" + textContent + "</div>"

def prepareDivTreeText(fileName):
    filePtr = open(fileName, 'r')
    textData = filePtr.read()
    textDataLines = textData.split('\n')

    htmlFile = "<html ng-app=\"TextSegmentation\" ng-controller=\"colorCtrl\">\n"
    htmlFile += includeJSLibraries()
    htmlFile += includeJSFile("segmentText.js")
    #htmlFile += prepareInstructions()

    htmlFile += "<div id=\"rootDiv\" ng-init=\"initApp()\">\n"
    for index in range(len(textDataLines)):
        if len(textDataLines[index]) == 0:
            continue
        htmlFile += prepareDiv(textDataLines[index], \
                                       "div" + str(index) + "color", False) + "\n"

        htmlFile += prepareDiv(textDataLines[index], \
                               str(index), True) + "\n"
        htmlFile += "\n<br>\n"


    htmlFile += "</div>\n"

    htmlFile += "<input type=\"button\" value=\"Segment Text\" ng-click=\"sendIndices()\"></input>"
    htmlFile += "<input type=\"button\" value=\"Next Segment\" ng-click=\"nextSegment()\"></input>"

    htmlFile += "</html>"
    return htmlFile

#print(prepareDivTreeText("Testfile.txt"))

