import os
from flask import Flask, request, redirect, url_for
from flask import Blueprint, render_template, abort, send_file, flash, send_from_directory
from werkzeug.utils import secure_filename
from jinja2 import TemplateNotFound
from prepareHTML import prepareDivTreeText

uploadBluePrint = Blueprint('upload', __name__, "/")

UPLOAD_FOLDER = './'
ALLOWED_EXTENSIONS = set(['txt', 'pdf', 'xls', 'docx'])

#upload_app = Flask(__name__)
#upload_app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
uploadBluePrint = Blueprint('uploadBluePrint', __name__)

downloadFolder = "./data"

def allowed_file(filename):
    return True
    #return '.' in filename and \
    #       filename.rsplit('.', 1)[1] in ALLOWED_EXTENSIONS

@uploadBluePrint.route('./upload')
def uploadedFile():
    if "filename" in request.args:
        fileName = request.args.get('filename')
        return prepareDivTreeText(downloadFolder + "/" + fileName)
        #return send_from_directory(downloadFolder,
        #                      filename)
    else:
        return "No file found, Please upload again!"

@uploadBluePrint.route('/', methods=['GET', 'POST'])
def uploadFile():
    print(request.method)
    if request.method == 'POST':
        # check if the post request has the file part
        print("printing request.files")
        if 'file' not in request.files:
            flash('No file part')
            return redirect(request.url)
        file = request.files['file']
        # if user does not select file, browser also
        # submit a empty part without filename
        if file.filename == '':
            flash('No selected file')
            return redirect(request.url)
        print("Everything went well\n")
        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
            file.save(os.path.join(downloadFolder, filename))
            return redirect(url_for('uploadBluePrint.uploadedFile',
                                    filename=filename))

    #No File Found
    #Find out how to send a user message
    return send_file(os.getcwd()+'/upload/upload.html')

@uploadBluePrint.route('/uploadTest')
def uploadTest():
    return "Testing inside upload App"
