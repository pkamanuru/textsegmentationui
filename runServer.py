from flask import Flask, send_from_directory
app = Flask(__name__)

#Importing the upload app
from upload.routes import uploadBluePrint
app.register_blueprint(uploadBluePrint, url_prefix="/upload")

#Importing segmentText app
from segment.routes import segmentBluePrint
app.register_blueprint(segmentBluePrint, url_prefix="/segment")

#Allows only javascript files securing the other files
#This is for the HTML pages to get JS files
@app.route('/JS/<path:path>')
def send_js(path):
    return send_from_directory("JS", path)

app.secret_key = "TextSegmentationIV"
app.run(debug=True)
