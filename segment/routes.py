import os, json
from flask import Flask, request, redirect, url_for
from flask import Blueprint, render_template, abort, send_file, flash, send_from_directory

segmentBluePrint = Blueprint('segment', __name__, "/")

@segmentBluePrint.route('/segmentText', methods=['POST'])
def segmentText():
    print("Printing the request hit in segmentText")

    #The input marked by the user which will used as data
    print(request.data)

    indices = [1]
    val = {}
    val["indices"] = indices

    #List of AI Generated Indices should be indeally sent
    return json.dumps(val)
