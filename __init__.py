from flask import Flask,Blueprint
from upload.uploadBluePrint import uploadBluePrint

app = Flask(__name__)
#upload = Blueprint('upload', __name__)
#app.register_blueprint(upload)

@app.route("/test")
def test():
    print("Hello World!\n")
    return "Testing inside App"


@app.route("/checkRules")
def checkRules():
    for rule in app.url_map.iter_rules():
        print(rule)
    return "Check Console for logs!"

app.run()
